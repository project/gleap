CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The module makes it easy to integrate with Gleap through the Gleap API key,
this will allow the customer to give their feedback in an intelligent way.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/issues/gleap

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/gleap


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------
The installation of this module is like other Drupal modules,
install the module and enable it.


Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

- Get the Gleap API key from https://app.gleap.io/ > Project > Settings > Installation.
- To Add Gleap API key from /admin/config/services/gleap.
